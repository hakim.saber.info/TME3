;;;; Exercice 4 : Feux de signalisation

#lang r5rs

(define (mk-feu)
  (define etat 'rouge)
  (lambda (msg)
    (case msg
      ('etat etat)
      ('change! (case etat
                  ('rouge (set! etat 'vert))
                  ('vert (set! etat 'orange))
                  ('orange (set! etat 'rouge))))
      ;; ('set! (lambda (e) (set! etat e)))
      (else 'invalid-msg))
  ))

(define feu (mk-feu))
;; (feu 'etat) → rouge
;; (feu 'change!) → #unspecified
;; (feu 'etat) → vert
;; (feu 'change!) → #unspecified
;; (feu 'etat) → orange
;; (feu 'change!) → #unspecified
;; (feu 'etat) → rouge
