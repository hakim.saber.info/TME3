;;;; Exercice 2 : Paires mutables

#lang r5rs

;;; Q1

(define couple (list 'un 1))
(set-car! couple 'deux)
(set-cdr! couple (list 2))

;;; Q2

(define ma-liste-1 (list 2 3 4 5))
(define ma-liste-2 (list 6 7 8 9))

(define (cons! e l)
  ;; On veut "décaler l vers la droite" en place
  ;; pour cela on utilise set-cdr! : la queue de l
  ;; devient l
  ;; On ne peut pas faire (set-cdr! l l) sinon cela
  ;; crée une boucle de références
  (set-cdr! l (cons (car l) (cdr l)))
  (set-car! l e))

;; (if cond (begin e1 e2 ...) else)

(cons! 1 ma-liste-1)

;;; Q3

(define (destruct! l)
  (let ((x (car l)))
    (set-car! l (cadr l))
    (set-cdr! l (cddr l))
    x))

(destruct! ma-liste-1)

;;; Q4

;;; der-paire: la dernière paire de la liste l
;;; Précondition: l est non vide
(define (der-paire l)
  (if (pair? (cdr l))
      ;; l est de la forme '(e1 (e2 tl))
      (der-paire (cdr l))
      ;; sinon l est de la forme '(e1 ())
      l))

(define (concat! l1 l2)
  (set-cdr! (der-paire l1) l2))

(concat! ma-liste-1 ma-liste-2)

;;; Q5

(concat! ma-liste-1 ma-liste-1)
